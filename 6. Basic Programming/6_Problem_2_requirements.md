# Basic Python Programming

## Case study: Invoices' due dates

In data folder find a excel report named `invoices` with the following sheets:

`fixed_holidays` {month, day, name} 

Ex. `12,25,Christmas`

`variable_holidays` {date, name }

Ex `'2021-04-17', 'Holiday 1'`

`invoices` { id, date, due_days}


### Write the python scripts for determining the due date for each invoice, saving the dataframe as a new sheet for the current file. 

`invoices_due_date`{ id, date, due_days, due_date}