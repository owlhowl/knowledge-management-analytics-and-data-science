# Basic Python Programming

## Case study: Master Programmes Admission (simplified version - no positions for tuition fee studies)

Data: two data frames stored as csv files (see `data` folder in this file's parent folder) - `applicants.csv` and `master_progs.csv`

Data frame `master_progs` stores every program abbreviation, name, and the number of available positions:

master_progs
- prog_abbreviation
- prog_name
- n_of_positions

Data frame `applicants` stores information about applicants:
- "prog1_abbreviation", "prog2_abbreviation", … describes applicant preferences (she/he wants to be accepted at "prog1_abbreviation", but if her/his results are not good enough for this program, she/he
would prefer "prog2_abbreviation", etc.)
- "grades_avg" refers to applicant's grades average (first part of the acceptance criteria for all master programmes)
- "dissertation_avg" refers to applicant's dissertation average (the other part of the acceptance criteria for all master programmes) applicants:
- applicant_id
- applicant_name

### Write the python scripts for assigning each applicant to one of her/his programme options, according to the points average and create data frame `results`:

`results`
- applicant_id
- prog_abbreviation_accepted

Students ranking depends on the applicants' admission average points which are computed as grades_avg * 0.6 + dissertation_avg * 0.4